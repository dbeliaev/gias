<?php

namespace k2\Parsers\BelarusRnpBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use k2\Parsers\BelarusRnpBundle\Component\Collection;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ParseService
{
    /**
     * Конструктор
     */
    public function __construct(
        private readonly ParameterBagInterface $params,
        private readonly DocumentManager $doctrine,
        private readonly LoggerInterface $log
    )
    {
    }

    /**
     *  Забираем данные из апи
     *
     * @return \Generator
     * @throws GuzzleException
     */
    public function getFromUrl(): \Generator
    {
        $params = $this->params->get('belarusRnp.config');
        $client = new Client(['verify' => false ]);
        $response = [];
        $page = 0;
        do {
            $res = $client->get($params['url'], ['query' => ['page' => $page, 'size' => $params['size']]]);
            try {
                $response = json_decode($res->getBody(), true, JSON_THROW_ON_ERROR);
            }catch (\Throwable $exception) {
                $this->log->error('Can`t get response', ['exception' => $exception]);
            }
            yield($response['content']);
            $page++;

        } while (!$response['last']);
    }

    /**
     * Подготавливаем данные для сохранения в бд
     *
     * @param array $data
     * @return array
     */
    public function prepareForSaving(array $data):array
    {
        array_walk($data, function (&$value) {
           $value['hash'] =  md5(serialize($value));
        });

        return $data;
    }

    /**
     * Сохраняем данные в бд
     *
     * @param array $data
     * @return void
     */
    public function saveToDb(array $data): void
    {
        $params = $this->params->get('belarusRnp.config');
        $data = $this->prepareForSaving($data);
        $collection = new Collection($this->doctrine, $params['db'], $params['collection']);
        foreach ($data as $key) {
            try {
                if (!$collection->exist(['hash' => $key['hash']])) {
                    $collection->insert($key);
                }
            }catch (\MongoDB\Driver\Exception\Exception $e) {
                $this->log->error('Can`t insert data', ['exception' => $e]);
            }
        }

    }

}
<?php

namespace k2\Parsers\BelarusRnpBundle\Component;

use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\Driver\Command;
use MongoDB\Driver\Exception\Exception;
use MongoDB\Driver\Manager;
use MongoDB\InsertOneResult;
use MongoDB\UpdateResult;

class Collection
{
    /** @var Manager База данных с которой работаем */
    private Manager $db;

    /**
     * Конструктор
     *
     * @param DocumentManager $doctrine
     * @param string $databaseName
     * @param string $collectionName
     */
    public function __construct(
        private readonly DocumentManager $doctrine,
        private readonly string          $databaseName,
        private readonly string          $collectionName,
    )
    {
    }

    /**
     * Проверка существования записи
     *
     * @param array $filter Фильтр для выборки данных
     *
     * @return bool
     *
     * @throws Exception
     */
    public function exist(array $filter): bool
    {
        $result = $this->getDb()
            ->executeCommand($this->databaseName, new Command(["count" => $this->collectionName, "query" => $filter]))
            ->toArray()[0];
        return $result->ok === 1.0 && $result->n === 1;
    }
    /**
     * Получение коллекции, с которой работаем
     *
     * @return \MongoDB\Collection
     */
    public function getCollection(): \MongoDB\Collection
    {
        if (!isset($this->collection)) {
            $this->collection = $this->doctrine->getClient()->selectCollection($this->databaseName, $this->collectionName);
        }
        return $this->collection;
    }

    /**
     * Получение данных документа
     *
     * @param array $filter фильтр поиска документа
     * @param array $fields получаемые поля документа. Если не задано - все
     *
     * @return array|null
     */
    public function find(array $filter, array $fields = []): ?array
    {
        if (!empty($fields)) {
            $fields[] = '_id';
            $fields = ['projection' => array_fill_keys(array_unique($fields), 1)];
        }
        $result = $this->getCollection()->find($filter, $fields)->toArray();
        return empty($result) ? null : $result;
    }

    /**
     * Изменение или добавление документа в коллекции.
     *
     * @param array $filter фильтр для поиска документа
     * @param array $document Данные документа для добавления
     *
     * @return UpdateResult|InsertOneResult
     * @throws Exception
     */
    public function upsert(array $filter, array $document): UpdateResult|InsertOneResult
    {
        if ($this->exist($filter)) {
            return $this->update($filter, $document);
        }
        return $this->insert($document);
    }

    /**
     * Добавление документа в коллекцию
     *
     * @param array $document Данные документа для добавления
     *
     * @return InsertOneResult
     */
    public function insert(array $document): InsertOneResult
    {
        return $this->getCollection()->insertOne($document);
    }

    /**
     * Изменение документа в коллекции.
     *
     * @param array $filter фильтр для поиска документа
     * @param array $document Данные документа для изменения
     * @param array $options Дополнительные параметры вставки
     *
     * @return UpdateResult
     */
    public function update(array $filter, array $document, array $options = []): UpdateResult
    {
        return $this->getCollection()->updateOne($filter, ['$set' => $document], $options);
    }

    /**
     * Замена документа в коллекции.
     *
     * @param array $filter фильтр для поиска документа
     * @param array $document Данные документа для замены
     * @param array $options Дополнительные параметры вставки
     *
     * @return UpdateResult
     */
    public function replace(array $filter, array $document, array $options = []): UpdateResult
    {
        return $this->getCollection()->replaceOne($filter, $document, $options);
    }

    /**
     * Получение подключения к БД
     *
     * @return Manager
     */
    private function getDb(): Manager
    {
        if (!isset($this->db)) {
            $this->db = $this->doctrine->getClient()->getManager();
        }
        return $this->db;
    }
}
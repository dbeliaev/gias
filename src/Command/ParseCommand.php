<?php

namespace k2\Parsers\BelarusRnpBundle\Command;

use k2\Parsers\BelarusRnpBundle\Service\ParseService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name:'belarus:rnp:parse')]
class ParseCommand extends Command
{
    /**
     * Конструктор
     *
     * @param ParseService $service
     */
    public function __construct(
        private readonly ParseService $service
    )
    {
        parent::__construct();
    }

    /**
     * Команда на выполнение
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
          foreach ($this->service->getFromUrl() as $row) {
              $this->service->saveToDb($row);
          }
        return Command::SUCCESS;
    }
}
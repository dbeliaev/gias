# belarus-rnp
***
Парсер данных ГИАС

Сервис работает как symfony-bundle
Парсинг производится в MongoDB

## Конфигурация
Сервис работает как Symfony Framework Bundle

1. Подключаем сервис к приложению
> /config/services.yaml

```yaml
    k2\Parsers\BelarusRnpBundle\:
      resource: '../vendor/k2/belarus-rnp/src/'
```

2. Устанавливаем необходимые параметры

```yaml
parameters:
    belarusRnp.config:
        url: 'http://gias.by/directory/api/v1/locked_suppliers/page'
        size: 30 # Размер записей которые достаются за раз
        db: 'rnp' # Название Базы Данных
        collection: 'belarus' # Название коллекции
```


***
#### Команда для парсинга:

> php bin/console belarus:rnp:parse
